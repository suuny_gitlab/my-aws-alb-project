
resource "aws_security_group" "ec2_sg" {
  name        = "ec2_security_group"
  description = "Allow HTTP and HTTPS traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 9081
    to_port     = 9081
    protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
    security_groups = [ aws_security_group.alb_sg.id ]
  }

  ingress {
    from_port   = 9082
    to_port     = 9082
    protocol    = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
    security_groups = [ aws_security_group.alb_sg.id ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ec2_security_group"
  }
}
