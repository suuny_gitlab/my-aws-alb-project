resource "aws_lb_target_group" "examplehttps" {
  name     = "example-https-tg"
  port     = var.https_port
  protocol = "HTTPS"
  vpc_id   = var.vpc_id

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 5
    interval            = 30
    port                = 9082
    protocol            = "HTTPS"
    path                = "/abc/xyz.conf"
    matcher             = "200"
  }

  tags = {
    Name = "example-https-tg"
  }
}
