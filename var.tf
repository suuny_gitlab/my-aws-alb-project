variable "vpc_id" {
  description = "The VPC ID where the ALB and other resources will be deployed"
  type        = string
}

variable "subnets" {
  description = "A list of subnets to associate with the ALB"
  type        = list(string)
}



variable "http_port" {
  description = "A list of subnets to associate with the ALB"
  type        = number
}

variable "https_port" {
  description = "A list of subnets to associate with the ALB"
  type        = number
}

variable "instance_ids" {
  description = "List of EC2 instance IDs to attach to the target group"
  type        = list(string)
}


variable "add_ports" {
  description = "list of ports"
  type = list(number)
  default = []
}