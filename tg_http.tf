resource "aws_lb_target_group" "example" {
  name     = "example-tg"
  port     = var.http_port
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 5
    interval            = 30
    port                = 9081
    protocol            = "HTTP"
    path                = "/abc/xyz.conf"
    matcher             = "200"
  }

  tags = {
    Name = "example-tg"
  }
}

resource "aws_lb_target_group_attachment" "example" {
  for_each            = toset(var.instance_ids)
  target_group_arn    = aws_lb_target_group.example.arn
  target_id           = each.value
  port                = 9081
}